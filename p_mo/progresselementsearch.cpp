#include "functions.h"

/**
 Функция получает на вход первое число прогрессии типа double, шаг прогрессии типа double и номер элемента прогрессии типа int.
 \nВозращает значение элемента типа double.
*/
double ProgressElementSearch(double first, double step, int i)
{
    return first + step*(i-1);

}
