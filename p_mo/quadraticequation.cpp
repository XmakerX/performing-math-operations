#include <QVector>
#include "math.h"
#include "functions.h"

/**
 Функция получает на вход массив из трех чисел типа float, значение ошибки типа bool.
 \nНичего не возвращает
*/
void QuadraticEquation(QVector<float> &vec, bool &error)
{

    float D = vec[1]*vec[1] - 4*vec[0]*vec[2];
    if (D >= 0){
        if (vec[0] == 0){
        vec[0] = vec[1];
        vec[1] = vec[2];

        float x1 = LinearEquation(vec,error);
        vec.clear();
        vec.append(x1);
        vec.append(x1);
        }
        else {


       float x1 = (-vec[1]+sqrt(D))/(2*vec[0]);
       float x2 = (-vec[1]-sqrt(D))/(2*vec[0]);
       vec.clear();
       vec.append(x1);
       vec.append(x2);
       }
    }
    else {
        error = true;
    }

}
