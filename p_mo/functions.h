#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <QVector>

/**
\file
\brief Выполнение некоторых математических операций
\author Sasha White
\version 1.0
\date 07.06.2019
*/

/**
 * \brief Функция для нахождения корней линейного уравнения.
 */
float LinearEquation(QVector<float> &vec, bool &error);

/**
 * \brief Функция для нахождения корней квадратного уравнения.
*/
void QuadraticEquation(QVector<float> &vec, bool &error);

/**
 * \brief Функция для нахождения корней кубического уравнения.
*/
void CubicEquation(QVector<double> &vec, int &error);

/**
 * \brief Функция для нахождения элемента арифметической прогрессии.
*/
double ProgressElementSearch(double first, double step, int i);

/**
 * \brief Функция для нахождения суммы элементов арифметической прогрессии.
*/
double SummElementsProgress(double first, double step, int size);

/**
 * \brief Функция для нахождения суммы элементов геометрической прогрессии.
*/
double SummOfGeometicProgress(double first, double step, int size, bool &error);

/**
 * \brief Функция для нахождения суммы двух чисел.
*/
double Summ(double first,double second);

/**
 * \brief Функция для нахождения разности двух чисел.
*/
double Difference(double first,double second);

#endif // FUNCTIONS_H
