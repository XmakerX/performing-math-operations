#include "functions.h"

/**
 Функция получает на вход два числа типа double.
 \nВозвращает число типа double.
*/
double Summ(double first,double second)
{
    return first + second;
}
