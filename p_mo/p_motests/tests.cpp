#include <QtTest>
#include "tests.h"
#include <../functions.h>



bool compareVec(const QVector<float> vec1, const QVector<float> &vec2) {
    if (vec1 == vec2)
        return true;
    return false;
}


bool compareVecDouble(const QVector<double> vec1, const QVector<double> &vec2, bool full) {
    if ((!full and vec1[0] == vec2[0] and vec1[1] == vec2[1]) or
           (full and vec1 == vec2) )
        return true;
    return false;
}

Tests::Tests()
{

}


void Tests::test_case1()
{
    bool error = false;
    QVector<float> test_vec_before = {1, -1};
    QCOMPARE(LinearEquation(test_vec_before,error), 1);

    test_vec_before = {0.5, -1};
    QCOMPARE(LinearEquation(test_vec_before,error),2);

    test_vec_before = {0, -1};
    LinearEquation(test_vec_before,error);
    QCOMPARE(true,error);
    error = false;

    test_vec_before = {1, 0};
    QCOMPARE(LinearEquation(test_vec_before,error),0);


}

void Tests::test_case2()
{
    bool error = false;
    QVector<float> test_vec_before = {0, 2, 2};
    QuadraticEquation(test_vec_before, error);
    QVector<float> test_vec_after = {-1, -1};
    QCOMPARE(true, compareVec(test_vec_before, test_vec_after));

    error = false;
    test_vec_before = {1, 1, 4};
    QuadraticEquation(test_vec_before, error);
    QCOMPARE(true, error);


}

void Tests::test_case3()
{
    int error = 0;
    QVector<double> test_vec_before = {2, -10, 16,-8};
    CubicEquation(test_vec_before, error);
    QVector<double> test_vec_after = {1, 2};
    QCOMPARE(true, compareVecDouble(test_vec_before, test_vec_after,false));

    error = 0;
    test_vec_before = {0, 1, 4, 5};
    CubicEquation(test_vec_before, error);
    QCOMPARE(1, error);

    error = 0;
    test_vec_before = {3, 1, -8, 4};
    CubicEquation(test_vec_before, error);
    test_vec_after = {-2, 1,-0.521};
    QCOMPARE(true, compareVecDouble(test_vec_before, test_vec_after,true));


}

void Tests::test_case4()
{

    double test_vec_before = ProgressElementSearch(1,2,4);
    double test_vec_after = 7;
    QCOMPARE(test_vec_before, test_vec_after);

    test_vec_before = ProgressElementSearch(5.3,5,7);
    test_vec_after = 35.3;
    QCOMPARE(test_vec_before, test_vec_after);

    test_vec_before = ProgressElementSearch(0,4.5,7);
    test_vec_after = 27;
    QCOMPARE(test_vec_before, test_vec_after);


}


void Tests::test_case5()
{

    double test_vec_before = SummElementsProgress(1,2,4);
    double test_vec_after = 16;
    QCOMPARE(test_vec_before, test_vec_after);

    test_vec_before = SummElementsProgress(5.3,5,7);
    test_vec_after = 142.1;
    QCOMPARE(test_vec_before, test_vec_after);

    test_vec_before = SummElementsProgress(0,4.5,7);
    test_vec_after = 94.5;
    QCOMPARE(test_vec_before, test_vec_after);


}

void Tests::test_case6()
{
    bool error = false;
    double test_vec_before = SummOfGeometicProgress(1,2,4,error);
    double test_vec_after = 15;
    QCOMPARE(test_vec_before, test_vec_after);

    test_vec_before = SummOfGeometicProgress(5.3,5,7,error);
    test_vec_after = 103514.3;
    QCOMPARE(test_vec_before, test_vec_after);

    test_vec_before = SummOfGeometicProgress(1,1,7,error);
    test_vec_after = 0;
    QCOMPARE(true, error);


}

void Tests::test_case7()
{

    QCOMPARE(Summ(5,5), 10);
    QCOMPARE(Summ(7.4,8.2),15.6);



}
void Tests::test_case8()
{
    QCOMPARE(Difference(7.4,8.2),-0.8);
    QCOMPARE(Difference(9,8),1);

}
