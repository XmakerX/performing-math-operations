QT += testlib
QT -= gui

TARGET = p_motests
CONFIG += console testcase
CONFIG -= app_bundle

TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS


SOURCES += \
    main.cpp \
    tests.cpp \
    ../linearequation.cpp \
    ../quadraticequation.cpp \
    ../cubicequation.cpp \
    ../progresselementsearch.cpp \
    ../summelementsprogress.cpp \
    ../summofgeometricprogress.cpp \
    ../summa.cpp \
     ../difference.cpp


HEADERS += \
    tests.h \
    ../functions.h

