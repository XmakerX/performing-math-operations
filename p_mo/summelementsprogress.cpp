#include "functions.h"

/**
 Функция получает на вход первое число прогрессии типа double, шаг прогрессии типа double и номер элемента прогрессии типа int.
 \nВозвращает число типа double.
*/
double SummElementsProgress(double first, double step, int size)
{
    double an = first + step*(size-1);
    return (first + an)*size/2;

}
