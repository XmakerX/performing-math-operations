#include <QVector>
#include "functions.h"

/**
 Функция получает на вход массив из двух чисел типа float, значение ошибки типа bool.
 \nВозвращает число типа float.
*/
float LinearEquation(QVector<float> &vec, bool &error)
{
    //ax + b = 0
    if(vec[0] != 0)
    return -vec[1]/vec[0];
    else
    {
    error = true;
    return 0;
    }

}
