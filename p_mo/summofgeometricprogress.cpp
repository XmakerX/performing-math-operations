#include "math.h"
#include "functions.h"

/**
 Функция получает на вход первое число прогрессии типа double, шаг прогрессии типа double, номер элемента прогрессии типа int, наличие ошибки типа bool.
 \nВозвращает число типа double.
*/
double SummOfGeometicProgress(double first, double step, int size, bool &error)
{
    if (step == 1)
    {error = true; return 0;}
    else {
        return (first*(1 - pow(step,size)))/(1-step);
    }
}
